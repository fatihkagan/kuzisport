//
//  KarateSnapshotTests.swift
//  KarateSnapshotTests
//
//  Created by fatih on 22/08/2019.
//  Copyright © 2019 Fatih Kagan Emre. All rights reserved.
//

import FBSnapshotTestCase
@testable import Karate

class KarateSnapshotTests: FBSnapshotTestCase {

    override func setUp() {
        super.setUp()
        self.recordMode = false
        fileNameOptions.insert(.device)
    }

    func testExample() {
        let controller = ControllerBuilder().build(.info)
        controller.view.frame = UIScreen.main.bounds
        FBSnapshotVerifyView(controller.view)
        FBSnapshotVerifyLayer(controller.view.layer)
    }
}
